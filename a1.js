let http = require("http");

http.createServer(function (request, response) {

    if(request.url == "" && request.method == "GET"){
        response.writeHead(200,{'Content-Type': 'application/json'});
        response.end('Welcome to Booking System!');
    }

   if(request.url == "/profile" && request.method == "GET"){

    response.writeHead(200, {"Content-Type": "application/json"});
    response.end("Welcome to your profile!");
   }

   if(request.url == "/courses" && request.method == "GET"){

    response.writeHead(200,{'Content-Type': 'application/json'});
    response.end("Here's our courses available");
    }

    // POST
   if(request.url == "/addcourse" && request.method == "POST"){

        response.writeHead(200,{'Content-Type': 'application/json'});
        response.end("Add a course to our resources");
    }

    // PUT
   if(request.url == "/updatecourse" && request.method == "PUT"){

    response.writeHead(200,{'Content-Type': 'application/json'});
    response.end("Update a course to our resources");
    }

    // DELETE/ARCHIVE
   if(request.url == "/archivecourse" && request.method == "DELETE"){

    response.writeHead(200,{'Content-Type': 'application/json'});
    response.end("Archive a course to our resources");
    }


}).listen(4000);

console.log('Server running at localhost:4000');